package main

type operacao interface {
	calculo(float64, float64) (float64, error);
}
