package main

import "math"

type raizQua struct {
	number
}

func (raizQua) calculo(num1, num2 float64) (float64, error) {
	c := math.Sqrt(num1)
	return c, nil
}
