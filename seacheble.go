package main

import "fmt"

var oper = map[string]operacao {
	"sum": sumNumber{},
	"+": sumNumber{},
	"mul":multNumber{},
	"*": multNumber{},
	"sub": subNumber{},
	"-": subNumber{},
	"div": divNumber{},
	"/": divNumber{},
	"pow": potNumber{},
	"^": potNumber{},
	"rot": raizQua{},
	"&": raizQua{},
}

func run() {
	var userInput1, userInput3 float64
	userInput1 = getUserNum("Digite o número: ")
	userInput2 := getUser("Digite a opção de operação: ")
	userInput3 = getUserNum("Digite outro número: ")
	
	teste := verifi(userInput2)
	if (!teste){
		fmt.Println("Valor de operação não disponivel")
		return
	}
	
	s, errorr := oper[userInput2].calculo(userInput1, userInput3)
	
	fmt.Printf("%v erro: %v", s, errorr)

}

func getUser(mes string) (string) {
	var userInput3 string
	fmt.Println(mes)
	fmt.Scanf("%v", &userInput3)
	return userInput3
}

func getUserNum(mes1 string) (float64) {
	var userInput4 float64
	fmt.Println(mes1)
	fmt.Scanf("%v", &userInput4)
	return userInput4
}


func verifi(anao string) (bool) {
	return oper[anao] != nil
}

