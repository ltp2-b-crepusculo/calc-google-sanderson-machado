package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
	"strings"
	"strconv"
)

type MyHandler struct{}

func (my MyHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	afe := req.URL.Query().Get("op")

	if afe == "" {
		res.Write([]byte("Valor vazio"))
		return
	}

	num1, num2, opera, err := quebrer(afe)
	if err != nil {
		res.Write([]byte("Erro ao processar a requisição"))
		return
	}

	s, err := oper[opera].calculo(num1, num2)
	if err != nil {
		res.Write([]byte("Erro ao calcular"))
		return
	}

	fmt.Fprintf(res, "Resultado: %v", s)
}

func quebrer(input string) (float64, float64, string, error) {
	var ope string
	var parts []string
	
	for key, _ := range oper {
		if (strings.Contains(input, key)) {
			
			parts = strings.Split(input, key)
			parts = append(parts, key)
		}
		
	}
	x, _ := strconv.ParseFloat(parts[0], 64)
	y, _ := strconv.ParseFloat(parts[1], 64)
	ope = parts[2]
	
	
	return x, y, ope, nil
}

func StartServer() {
	s := &http.Server{
		Addr:         "10.0.2.15:8080",
		Handler:      MyHandler{},
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	log.Fatal(s.ListenAndServe())
}

