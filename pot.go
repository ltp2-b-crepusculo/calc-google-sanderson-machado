package main

import "math"

type potNumber struct {
	number
}

func (potNumber) calculo(num1, num2 float64) (float64, error) {
	y := math.Pow(num1, num2)
	return y, nil
}
