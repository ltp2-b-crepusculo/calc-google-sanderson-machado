package main

type divNumber struct {
	number
}

func (divNumber) calculo(num1, num2 float64) (float64, error) {
	return num1 / num2, nil
}
