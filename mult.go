package main

type multNumber struct {
	number
}

func (multNumber) calculo(num1, num2 float64) (float64, error) {
	return num1 * num2, nil
}
