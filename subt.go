package main

type subNumber struct {
	number
}

func (subNumber) calculo(num1, num2 float64) (float64, error) {
	return num1 - num2, nil
}
